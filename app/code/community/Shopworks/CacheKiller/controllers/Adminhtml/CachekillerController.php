<?php

class Shopworks_Cachekiller_Adminhtml_CachekillerController extends Mage_Adminhtml_Controller_Action 
{
    /*
    public function flushJSAction()
    {
        $self::flush('js');
    }

    public function flushCSSAction()
    {
        $self::flush('css');
    }
    */
    
    public function flushAllAction()
    {
        $this::flush('all');
    }
    
    private function flush($type)
    {
        Mage::helper('shopworks_cachekiller')->flushTimestamp($type);
        
        // clear config cache, otherwise new timestamp won't get used
        Mage::app()->getCacheInstance()->cleanType('config');
        
        $this->_redirect('*/cache');
    }

    protected function _isAllowed()
    {
        //return Mage::getSingleton('admin/session')->isAllowed('shopworks/cachekiller');
        return true;
    }
}