<?php
/**
 * Created by PhpStorm.
 * User: ron
 * Date: 7-11-2015
 * Time: 13:36
 */


/**
 * Merge specified javascript files and return URL to the merged file on success
 *
 * @param $files
 * @return string
 */
class Shopworks_CacheKiller_Model_Design_Package extends Mage_Core_Model_Design_Package
{
    public function getMergedJsUrl($files)
    {
        $helper = Mage::helper('shopworks_cachekiller');
        
        $jsParameterName = $helper->getJsParameterName();
        
        $jsEnabled = $helper->isJSEnabled();
        $jsTimestamp = $helper->getJSTimestamp();
        $useJSCompiledVersion = $helper->isJSCompiledEnabled();
        $JSCompiledPostfix = $helper->getJSCompiledPostfix();

        $targetFilename = md5(implode(',', $files)) . '.js';
        $targetDir = $this->_initMergerDir('js');
        if (!$targetDir) {
            return '';
        }
        if ($this->_mergeFiles($files, $targetDir . DS . $targetFilename, false, null, 'js')) {
            return Mage::getBaseUrl('media', Mage::app()->getRequest()->isSecure()) . 'js/' . $targetFilename . ($jsEnabled ? "?$jsParameterName=$jsTimestamp" : '');
        }
        return '';
    }

    /**
     * Merge specified css files and return URL to the merged file on success
     *
     * @param $files
     * @return string
     */
    public function getMergedCssUrl($files)
    {
        $helper = Mage::helper('shopworks_cachekiller');
        
        $cssParameterName = $helper->getCSSParameterName();
        
        $cssEnabled = $helper->isCSSEnabled();
        $cssTimestamp = $helper->getCSSTimestamp();

        // secure or unsecure
        $isSecure = Mage::app()->getRequest()->isSecure();
        $mergerDir = $isSecure ? 'css_secure' : 'css';
        $targetDir = $this->_initMergerDir($mergerDir);
        if (!$targetDir) {
            return '';
        }

        // base hostname & port
        $baseMediaUrl = Mage::getBaseUrl('media', $isSecure);
        $hostname = parse_url($baseMediaUrl, PHP_URL_HOST);
        $port = parse_url($baseMediaUrl, PHP_URL_PORT);
        if (false === $port) {
            $port = $isSecure ? 443 : 80;
        }

        // merge into target file
        $targetFilename = md5(implode(',', $files) . "|{$hostname}|{$port}") . '.css';
        $mergeFilesResult = $this->_mergeFiles(
            $files, $targetDir . DS . $targetFilename,
            false,
            array($this, 'beforeMergeCss'),
            'css'
        );
        if ($mergeFilesResult) {
            return $baseMediaUrl . $mergerDir . '/' . $targetFilename . ($cssEnabled ? "?$cssParameterName=$cssTimestamp" : '');
        }
        return '';
    }

}