<?php

class Shopworks_CacheKiller_Helper_Data extends Mage_Core_Helper_Abstract {

    const XML_PATH_CACHEKILLER_JS_TIMESTAMP = 'dev/js/cachekiller_timestamp';
    const XML_PATH_CACHEKILLER_JS_ENABLED = 'dev/js/cachekiller_enabled';

    const XML_PATH_DEV_JS_COMPILED_ENABLED = 'dev/js/compiled_enabled';
    const XML_PATH_DEV_JS_COMPILED_PREFIX = 'dev/js/compiled_prefix';
    const XML_PATH_DEV_JS_ARGUMENT_NAME = 'dev/js/parameter_name';
    
    const XML_PATH_CACHEKILLER_CSS_TIMESTAMP = 'dev/css/cachekiller_timestamp';
    const XML_PATH_CACHEKILLER_CSS_ENABLED = 'dev/css/cachekiller_enabled';

    const XML_PATH_DEV_CSS_COMPILED_ENABLED = 'dev/css/compiled_enabled';
    const XML_PATH_DEV_CSS_COMPILED_PREFIX = 'dev/css/compiled_prefix';
    const XML_PATH_DEV_CSS_ARGUMENT_NAME = 'dev/css/parameter_name';

    public $jsEnabled = false;
    public $cssEnabled = false;

    public function __construct()
    {
        $this->jsEnabled = (bool) Mage::getStoreConfig(self::XML_PATH_CACHEKILLER_JS_ENABLED);
        $this->cssEnabled = (bool) Mage::getStoreConfig(self::XML_PATH_CACHEKILLER_CSS_ENABLED);
    }

    public function isJSEnabled()
    {
        return $this->jsEnabled;
    }

    public function isJSCompiledEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEV_JS_COMPILED_ENABLED);
    }

    public function getJSTimestamp()
    {
        return Mage::getStoreConfig(self::XML_PATH_CACHEKILLER_JS_TIMESTAMP);
    }

    public function getJSCompiledPostfix()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEV_JS_COMPILED_PREFIX);
    }
    
    public function getJSParameterName()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEV_JS_ARGUMENT_NAME);
    }

    public function isCSSEnabled()
    {
        return $this->cssEnabled;
    }

    public function isCSSCompiledEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEV_CSS_COMPILED_ENABLED);
    }

    public function getCSSTimestamp()
    {
        return Mage::getStoreConfig(self::XML_PATH_CACHEKILLER_CSS_TIMESTAMP);
    }

    public function getCSSCompiledPostfix()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEV_CSS_COMPILED_PREFIX);
    }
    
    public function getCSSParameterName()
    {
        return Mage::getStoreConfig(self::XML_PATH_DEV_CSS_ARGUMENT_NAME);
    }


    public function flushTimestamp($type)
    {
        $timestamp = date('YmdHis');

        switch($type)
        {
            case 'js':
                Mage::getModel('core/config')->saveConfig(self::XML_PATH_CACHEKILLER_JS_TIMESTAMP, $timestamp);
                Mage::getSingleton('core/session')->addSuccess('New timestamp generated for JS files.');
                break;
            case 'css':
                Mage::getModel('core/config')->saveConfig(self::XML_PATH_CACHEKILLER_CSS_TIMESTAMP, $timestamp);
                Mage::getSingleton('core/session')->addSuccess('New timestamp generated for CSS files.');
                break;
            case 'all':
                Mage::getModel('core/config')->saveConfig(self::XML_PATH_CACHEKILLER_JS_TIMESTAMP, $timestamp);
                Mage::getModel('core/config')->saveConfig(self::XML_PATH_CACHEKILLER_CSS_TIMESTAMP, $timestamp);
                Mage::getSingleton('core/session')->addSuccess('New timestamp generated for CSS and JS files.');
                break;
        }

    }
}