<?php

require_once 'abstract.php';

/**
 * Shell script; see usageHelp for options
 */
class Shopworks_CacheKiller_Shell extends Mage_Shell_Abstract
{

    private $typeDefault = 'all';

    /**
     * Run shell script
     */
    public function run()
    {
        $helper = Mage::helper('shopworks_cachekiller');

        if ($this->getArg('flush'))
        {
            $type = $this->getArg('type');
            if (!$type)
            {
                $type = $this->typeDefault;
            }

            $helper->flushTimestamp($type);
            
            // clear config cache, otherwise new timestamp won't get used
            Mage::app()->getCacheInstance()->cleanType('config');

            echo "Typestamp has been flushed for $type. \n";
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     * @return string
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f shopworks_cachekiller.php -- [options]
        php -f shopworks_cachekiller.php -- flush --type all

  flush                     Flush timestamp
  --type <js, css, all>     Type to flush. js, css or all
  help                      This help

USAGE;
    }
}

$shell = new Shopworks_CacheKiller_Shell();
$shell->run();
